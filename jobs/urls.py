from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from timeclock.views import MyRegistrationBackend
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'jobs.views.home', name='home'),
    # url(r'^jobs/', include('jobs.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^users', RedirectView.as_view(url='/')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/register/', MyRegistrationBackend.as_view(), name='registration_register'),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^', include('jobboard.urls', namespace='jobboard')),
    url(r'^', include('timeclock.urls', namespace='timeclock')),
    
)
