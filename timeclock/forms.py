from django import forms
from django.forms import ModelForm
from django.contrib.admin import widgets
from timeclock.models import Employee, Clock

class AddEmployee(ModelForm):
	class Meta:
		model = Employee
		fields = ['name']

class AddClock(ModelForm):
	class Meta:
		model = Clock
		fields = ['clock_type','project', 'note']

class EditClock(ModelForm):
	class Meta:
		model = Clock
		fields = ['time', 'note']

class DateRangeForm(forms.Form):
	start_date = forms.DateField()
	end_date = forms.DateField()
