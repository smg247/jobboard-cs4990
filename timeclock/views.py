# Create your views here.
from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views.generic import ListView, DetailView, FormView, View
from django.views.generic.edit import CreateView, UpdateView
from django.contrib import messages
import datetime
from datetime import time, date, timedelta
from django.utils import timezone
from registration.backends.simple.views import RegistrationView
from django.views.generic.detail import SingleObjectMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from timeclock.models import Employee, Clock
from timeclock.forms import AddEmployee, AddClock, EditClock, DateRangeForm

class EmployeeList(ListView):
	context_object_name = 'employees'

	def get_queryset(self):
		user = self.request.user

		if int(user.id) == 1: #if the user is the admin show all employees
			return Employee.objects.all()

		else: #only show employees tied to the current user
			return Employee.objects.all().filter(user = user)
		
	def get_context_data(self, **kwargs):
		admin = False
		if self.request.user.id == 1:
			admin = True

		context = super(EmployeeList, self).get_context_data(**kwargs)
		context['admin'] = admin
		return context

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(EmployeeList, self).dispatch(*args, **kwargs)

class ClockDetail(DetailView):
	context_object_name = 'clock'
	template_name = 'timeclock/clock_detail.html'	

	def get_object(self):
		user = self.request.user
		clock = get_object_or_404(Clock, pk = self.kwargs['pk'])
		if user.id == clock.employee.user.id or self.request.user.id == 1:
			return clock
		else:
			return get_object_or_404(Clock, pk = 10000000000000000)#this will 404 which is what I want, I'm sure there is a better way to do this, but i dont know how

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(ClockDetail, self).dispatch(*args, **kwargs)
	
class ClockDisplay(ListView):
	context_object_name = 'clocks'
	template_name = 'timeclock/clock_list.html'
	def get_queryset(self):
		user = self.request.user
		employees = Employee.objects.all()
		start = employees[0].period_start#they will all be the same so why not use the first one?
		end = employees[0].period_end
		clocks = Clock.objects.all()
		if int(user.id) == 1: #if the user is the admin show all employees
			return clocks.filter(time__range=(start,end)).order_by('-time')

		else: #only show employees tied to the current user even though they shouldn't ever be here
			employee = Employee.objects.get(user = user)
			return Clock.objects.all().filter(employee = employee)

	def get_context_data(self, **kwargs):
		employees = Employee.objects.all()
		employee = employees[0]
		context = super(ClockDisplay, self).get_context_data(**kwargs)
		context['emp'] = employee
		context['form'] = DateRangeForm()
		return context

	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(ClockDisplay, self).dispatch(*args, **kwargs)

class ClockDateRange(SingleObjectMixin,FormView):
	template_name = 'timeclock/clock_list.html'
	form_class = DateRangeForm
	
	def get_queryset(self):
		return Employee.objects.all()

	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		return super(ClockDateRange, self).post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('timeclock:all_clocks', args=(1,))#I would choose to do this differently in the future

	def form_valid(self, form):
		employees = Employee.objects.all()
		for employee in employees:
			self.object.period_start = form.cleaned_data.get('start_date')
			self.object.period_end = form.cleaned_data.get('end_date')	
			self.object.save()
		return super(ClockDateRange, self).form_valid(form)

class ClockList(View):
	def get(self, request, *args, **kwargs):
		view = ClockDisplay.as_view()
		return view(request, *args, **kwargs)

	def post(self, request, *args, **kwargs):
		view = ClockDateRange.as_view()
		return view(request, *args, **kwargs)


class EmployeeDisplay(DetailView):
	context_object_name = 'employee'
	template_name = 'timeclock/employee_detail.html'

	def get_queryset(self):

		if self.request.user.id == self.kwargs['pk'] or self.request.user.id == 1: #if it is current user or admin
			return Employee.objects.filter(pk = self.kwargs['pk'])
		else:
			return Employee.objects.all().filter(user = self.request.user )#this will 404 which is perfectly fine

	def get_context_data(self, **kwargs):
		start = Employee.objects.get(pk = self.kwargs['pk']).period_start
		end = Employee.objects.get(pk = self.kwargs['pk']).period_end

		context = super(EmployeeDisplay, self).get_context_data(**kwargs)
		clocks = Clock.objects.all().filter(employee = Employee.objects.filter(pk = self.kwargs['pk'])).order_by('-time')
		clocks = clocks.filter(time__range=(start,end))
		context['clocks'] = clocks
		context['form'] = DateRangeForm()
		return context
	
	@method_decorator(login_required)
    	def dispatch(self, *args, **kwargs):
        	return super(EmployeeDisplay, self).dispatch(*args, **kwargs)


class DateRange(SingleObjectMixin,FormView):
	template_name = 'timeclock/employee_detail.html'
	form_class = DateRangeForm
	
	def get_queryset(self):
		return Employee.objects.filter(pk = self.kwargs['pk'])

	def post(self, request, *args, **kwargs):
		self.object = self.get_object()
		return super(DateRange, self).post(request, *args, **kwargs)

	def get_success_url(self):
		return reverse('timeclock:employee', args=(self.object.id,))

	def form_valid(self, form):
		employee = Employee.objects.filter(pk = self.kwargs['pk'])
		self.object.period_start = form.cleaned_data.get('start_date')
		self.object.period_end = form.cleaned_data.get('end_date')	
		self.object.save()
		return super(DateRange, self).form_valid(form)

	
class EmployeeDetail(View):
	def get(self, request, *args, **kwargs):
		view = EmployeeDisplay.as_view()
		return view(request, *args, **kwargs)

	def post(self, request, *args, **kwargs):
		view = DateRange.as_view()
		return view(request, *args, **kwargs)


class EmployeeCreate(CreateView):
	model = Employee
	template_name = 'timeclock/employee_create.html'
	form_class = AddEmployee
	
	def get_form(self, form_class):
		one_day_ago = timezone.now().date() - timedelta(days=1)
		tomorrow =  timezone.now().date() + timedelta(days=1)
		form = super(EmployeeCreate, self).get_form(form_class)
		form.instance.user = self.request.user
		form.instance.period_start = one_day_ago
		form.instance.period_end = tomorrow
		return form

class EmployeeEdit(UpdateView):
	model = Employee
	template_name = 'timeclock/employee_update.html'
	form_class = AddEmployee

	def get_form(self, form_class):
		form = super(EmployeeEdit, self).get_form(form_class)
		form.instance.user = self.request.user
		return form

class ClockCreate(CreateView):
	model = Clock
	template_name = 'timeclock/clock_create.html'
	form_class = AddClock

	def get_form(self, form_class):
		employee = get_object_or_404(Employee, pk = self.kwargs['pk'])
		form = super(ClockCreate, self).get_form(form_class)
		form.instance.employee = employee
		form.instance.time = timezone.now()
		return form

	def form_valid(self, form):
		employee = get_object_or_404(Employee, pk = self.kwargs['pk'])
		clocks = Clock.objects.all().filter(employee = employee).order_by('-pk')
		self.object = form.save(commit=False)
		if clocks: #making sure that users follow the rules
#			if self.object.clock_type == 'Clock Out':#update the total time on a clock out
#				if clocks[0].clock_type == 'Clock In':
#					total = self.object.time - clocks[0].time
#					hours, remainder = divmod(total.seconds, 3600)
#					minutes, seconds = divmod(remainder, 60)
#					total_time_string = "%02d:%02d:%02d" % (hours, minutes, seconds)
#					self.object.total_time = total_time_string
#					self.object.save()
#			else:#want total_time to be empty
#				self.object.total_time = ''
#				self.object.save()

			if not clocks[0].clock_type == self.object.clock_type:
				if clocks[0].clock_type == 'Clock In':
					if clocks[0].project == self.object.project:
						self.object.save()
						messages.success(self.request, str(self.object.clock_type) + ' successful!')
						return super(ClockCreate, self).form_valid(form)
					else:
						messages.error(self.request,'You tried to clock out on ' + str(self.object.project) + ' , but you are currently clocked in on ' + str(clocks[0].project))
				else:
					self.object.save()
					messages.success(self.request, str(self.object.clock_type) + ' successful!')
					return super(ClockCreate, self).form_valid(form)
			else:
				messages.error(self.request,'You tried to ' + str(self.object.clock_type) + ', but you already were.')
		
		else:
			self.object.save()
			messages.success(self.request, str(self.object.clock_type) + ' successful!')
			return super(ClockCreate, self).form_valid(form)

		return HttpResponseRedirect(reverse('timeclock:employee', args=(employee.id,)))

	def get_initial(self):
		employee = get_object_or_404(Employee, pk = self.kwargs['pk'])
		clocks = Clock.objects.all().filter(employee = employee).order_by('-pk')
		initial = super(ClockCreate, self).get_initial()
		initial = initial.copy()

		if clocks:
			if clocks[0].clock_type == 'Clock In':
				ct = 'Clock Out'
				proj = clocks[0].project
			else:
				ct = 'Clock In'
				proj = None
			initial['clock_type'] = ct
			initial['project'] = proj
		return initial

class ClockEdit(UpdateView):
	model = Clock
	template_name = 'timeclock/clock_update.html'
	form_class = EditClock
	
	def get_form(self, form_class):
		employee = get_object_or_404(Employee, pk = self.kwargs['employee_id'])
		form = super(ClockEdit, self).get_form(form_class)
		form.instance.employee = employee
		return form

	def form_valid(self, form):
		employee = get_object_or_404(Employee, pk = self.kwargs['employee_id'])
		clocks = Clock.objects.all().filter(employee = employee)
		self.object = form.save(commit=False)

		for i, clock in enumerate(clocks):
			if self.object.id == clock.id:
				index = i

		if self.object.time < timezone.now():
			if len(clocks) > index+1:
				if self.object.time < clocks[index+1].time:
					if index is not 0:#so much extra work just for the 0 case...
						if self.object.time > clocks[index-1].time:
							self.object.save()
							messages.success(self.request, str(self.object.clock_type) + ' edited successfully!')
							return super(ClockEdit, self).form_valid(form)
						else:
							messages.error(self.request, 'You tried to change the time of a ' + str(self.object.clock_type) + ' to a time that conflicts with an adjacent punch')
					else:
						self.object.save()
						messages.success(self.request, str(self.object.clock_type) + ' edited successfully!')
						return super(ClockEdit, self).form_valid(form)

				else:
					messages.error(self.request, 'You tried to change the time of a ' + str(self.object.clock_type) + ' to a time that conflicts with an adjacent punch')

			elif len(clocks) == 1:
				self.object.save()
				messages.success(self.request, str(self.object.clock_type) + ' edited successfully!')
				return super(ClockEdit, self).form_valid(form)

			elif index is not 0 and self.object.time > clocks[index-1].time:
				self.object.save()
				messages.success(self.request, str(self.object.clock_type) + ' edited successfully!')
				return super(ClockEdit, self).form_valid(form)

			else: 
				messages.error(self.request, 'You tried change the time of a ' + str(self.object.clock_type) + ' to a time that conflicts with a previous time.')

		else:
			messages.error(self.request, 'You tried change the time of a ' + str(self.object.clock_type) + ' to a future time.')

		return HttpResponseRedirect(reverse('timeclock:employee', args=(employee.id,)))
	
class MyRegistrationBackend(RegistrationView):
    def get_success_url(self, request, user):
        return reverse('timeclock:index')
			
def default_range(request, employee_id, ran):
	employee = get_object_or_404(Employee, pk = employee_id)

	two_weeks_ago = timezone.now().date() - timedelta(days=13)
	one_week_ago = timezone.now().date() - timedelta(days=6)
	one_month_ago = timezone.now().date() - timedelta(days=27)
	tommorrow =  timezone.now().date() + timedelta(days=1)

	if int(ran) == 2:
		employee.period_start = two_weeks_ago
	elif int(ran) == 1:
		employee.period_start = one_week_ago
	elif int(ran) == 4:
		employee.period_start = one_month_ago

	employee.period_end = tommorrow
	employee.save()
	return HttpResponseRedirect(reverse('timeclock:employee', args=(employee.id,)))	

def default_range_all(request, ran):
	employees = Employee.objects.all()
	two_weeks_ago = timezone.now().date() - timedelta(days=13)
	one_week_ago = timezone.now().date() - timedelta(days=6)
	one_month_ago = timezone.now().date() - timedelta(days=27)
	tommorrow =  timezone.now().date() + timedelta(days=1)
	for employee in employees:
		if int(ran) == 2:
			employee.period_start = two_weeks_ago
		elif int(ran) == 1:
			employee.period_start = one_week_ago
		elif int(ran) == 4:
			employee.period_start = one_month_ago

		employee.period_end = tommorrow
		employee.save()
	return HttpResponseRedirect(reverse('timeclock:all_clocks', args=(1,)))	
		
#functions that I either no longer need or didn't quite work the way I wanted them too, kept here for reference.
#def get_total_time(employee):
	#employee = get_object_or_404(Employee, id = employee_id)
#	clocks = Clock.objects.all().filter(employee = employee).order_by('-time')
#	length = len(clocks)
#	total_time = 0
#	print 'hello'
#	if clocks[0] == 'Clock Out':
#		print 'in if 1'
#		for i in clocks:
#			print 'in for'
#			if clocks[i].clock_type == 'Clock In':
#				print 'in if 2'
#				print total_time
#				total_time += clocks[i+1].time - clocks[i].time
#	return total_time

#def punch_clock(request, employee_id):
#	employee = get_object_or_404(Employee, pk=employee_id)
#	clocks = Clock.objects.all().filter(employee = employee.id).order_by('-time')
#	now = datetime.datetime.now()
#	last_type = clocks[0].clock_type
#	next_type = ''
#	if last_type is 'Clock In':
#		next_type = 'Clock Out'
#	else:
#		next_type = 'Clock In'
#	new_clock = Clock.objects.create(empoloyee = employee, clock_type = next_type, time = now)
#	
#	return HttpResponseRedirect(reverse('timeclock:employee', args=(employee.id,)))
#


		
