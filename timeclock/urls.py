from django.conf.urls import patterns, url
from timeclock import views
from timeclock.views import EmployeeList, EmployeeDetail, EmployeeCreate, ClockCreate, EmployeeEdit, ClockEdit, ClockDisplay, ClockList, ClockDetail

urlpatterns = patterns('',
	url(r'^timeclock/$', EmployeeList.as_view(), name='index'),
	url(r'^timeclock/employee/all/(?P<pk>\d+)/$', ClockList.as_view(), name='all_clocks'),
	url(r'^timeclock/employee/(?P<pk>\d+)/$', EmployeeDetail.as_view(), name='employee'),
	url(r'^timeclock/employee/add/$', EmployeeCreate.as_view(), name='add_employee'),
	url(r'^timeclock/employee/edit/(?P<pk>\d+)/$', EmployeeEdit.as_view(), name='edit_employee'),
	url(r'^timeclock/employee/(?P<pk>\d+)/clock/add/$', ClockCreate.as_view(), name='add_clock'),
	url(r'^timeclock/employee/(?P<employee_id>\d+)/clock/edit/(?P<pk>\d+)/$', ClockEdit.as_view(), name='edit_clock'),
	url(r'^timeclock/clock/(?P<pk>\d+)/$', ClockDetail.as_view(), name='clock'),
	url(r'^timeclock/default/(?P<employee_id>\d+)/(?P<ran>\d+)/$', views.default_range, name='default_range'),
	url(r'^timeclock/default/all/(?P<ran>\d+)/$', views.default_range_all, name='default_range_all'),
)
