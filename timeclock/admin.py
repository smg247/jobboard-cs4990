from django.contrib import admin

from timeclock.models import Employee, Clock, Project

class EmployeeAdmin(admin.ModelAdmin):
	search_fields = ['user','name']
	list_display = ('user','name','period_start','period_end',)

class ClockAdmin(admin.ModelAdmin):
	search_fields = ['employee','time']
	list_display = ('employee','clock_type','time', 'project',)

class ProjectAdmin(admin.ModelAdmin):
	search_fields = ['name']
	list_display = ('name',)

admin.site.register(Employee, EmployeeAdmin)
admin.site.register(Clock, ClockAdmin)
admin.site.register(Project, ProjectAdmin)
