from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

class Employee(models.Model):
	user = models.ForeignKey(User, unique=True)
	name = models.CharField(max_length=50)
	period_start = models.DateField(help_text= 'e.g. 10/09/2013')
	period_end = models.DateField(help_text= 'e.g. 10/09/2013')
	total_time = models.CharField(max_length=50, null=True)

	def get_absolute_url(self):
	        return reverse('timeclock:index')

	def __unicode__(self):
		return self.name

class Clock(models.Model):
	CLOCK_TYPES= (
	('Clock In', 'Clock In'),
	('Clock Out', 'Clock Out'),
	)

	employee = models.ForeignKey(Employee)
	clock_type = models.CharField('punch type',max_length=15, choices=CLOCK_TYPES)
	time = models.DateTimeField()
	project = models.ForeignKey('Project')
	total_time = models.CharField(max_length= 50, blank=True, null=True, editable=False)
	note = models.CharField(max_length= 200, blank=True, null=True)

	def get_absolute_url(self):
	        return reverse('timeclock:employee', args=(self.employee.id,))

	def __unicode__(self):
		return self.clock_type

class Project(models.Model):
	name = models.CharField(max_length=50)

	def __unicode__(self):
		return self.name

