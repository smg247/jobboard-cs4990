from django.conf.urls import patterns, include, url
from jobboard import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^job/(?P<job_id>\d+)/$', views.job_detail, name='job_detail'),
    url(r'^job_add/$', views.add_job, name='job_add'),
    url(r'^job/edit/(?P<job_id>\d+)/$', views.edit_job, name='job_edit'),
    url(r'^employer/(?P<employer_id>\d+)/$', views.employer_detail, name='employer_detail'),
    url(r'^employer_add/$', views.add_employer, name='employer_add'),
    url(r'^employer/edit/(?P<employer_id>\d+)/$', views.edit_employer, name='employer_edit'),
    url(r'^subscribe/$', views.add_subscriber, name='subscriber_add'),
    url(r'^unsubscribe/$', views.del_subscriber, name='subscriber_del'),
)
