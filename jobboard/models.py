
from django.db import models
from django.contrib.auth.models import User

class Employer(models.Model):
	user = models.ForeignKey(User)
	name = models.CharField(max_length=200)
	description = models.TextField()
	website = models.URLField(help_text= 'e.g. http://www.abc.com')
	email = models.EmailField('preferred contact email address')
	contact = models.CharField('contact name', max_length=30)

	def __unicode__(self):
                return self.name

class Job(models.Model):
	JOB_TYPES= (
	('Part Time', 'Part Time'),
	('Full Time', 'Full Time'),
	('Internship', 'Internship'),
	)	

	employer = models.ForeignKey(Employer)
	title = models.CharField(max_length=50)
	description = models.TextField()
	job_type = models.CharField(max_length=50, choices=JOB_TYPES)
	wage = models.CharField(max_length=50, help_text= 'e.g. $7.25 per hour, or $40,000 per year')
	pub_date = models.DateTimeField(auto_now_add=True)
	expiry_date = models.DateField(help_text= 'e.g. 10/09/2013')
	active = models.BooleanField()

	def __unicode__(self):
                return self.title

class Subscriber(models.Model):
	name = models.CharField(max_length=50)
	email = models.EmailField(unique=True)

	def __unicode__(self):
                return self.email
