from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.template import RequestContext, loader
import datetime
from django.utils import timezone

from jobboard.models import Employer, Job, Subscriber
from jobboard.forms import AddEmployer, AddJob, EditEmployer, EditJob, AddSubscriber, DelSubscriber, Contact

def index(request):
	unactivate() #do this first thing on the index page to make sure it is up to date
        user = request.user
        all_jobs = Job.objects.all().order_by('-pub_date')
        recent_jobs = all_jobs.filter(active=True)[:5]
        employers = Employer.objects.all()
        jobs = all_jobs.filter(active=True)
        context= {
        'user':user,
        'recent_jobs':recent_jobs,
        'employers':employers,
        'jobs':jobs,
        }
        return render(request, 'jobboard/index.html', context)

def job_detail(request, job_id):
	user = request.user
	employers = Employer.objects.all()
	all_jobs = Job.objects.all().order_by('-pub_date')
	recent_jobs = all_jobs.filter(active=True)[:5]
	job = get_object_or_404(Job, pk=job_id, active=True)#only show the page if the job is active
	form = Contact(request.POST or None)
	if form.is_valid():
		cd = form.cleaned_data
		name = cd.get('name')
		email = cd.get('email')
		phone = cd.get('phone')
		message = cd.get('message')
		contact = job.employer.contact
		title = job.title
		template = loader.get_template('jobboard/contact_email.txt')
		context = RequestContext(request, {'name':name, 'email':email, 'phone':phone, 'message':message, 'contact':contact, 'title':title, })
		body = template.render(context)
		send_mail('Message about posted job: on the CIT Job Board', body, 'no-reply@betterjobboard.com', [job.employer.email],fail_silently=False)
		messages.success(request, 'Your message has been sent to the employer.')
	elif form.errors:
		messages.error(request, 'Something went wrong, please try again.')
		
	context= {
	'user':user,
	'recent_jobs':recent_jobs,
	'employers':employers,
	'job':job,
	'form':form,
	}
	return render(request, 'jobboard/job_detail.html', context)

def employer_detail(request, employer_id):
	user = request.user
	employers = Employer.objects.all()
	all_jobs = Job.objects.all().order_by('-pub_date')
	recent_jobs = all_jobs.filter(active=True)[:5]
	employer = get_object_or_404(Employer, pk=employer_id)
	jobs = all_jobs.filter(employer__id=employer_id)#these are the jobs you will see if you are the user who made the employer
	cur_user = False
	if(user.id > 0):#just checking to make sure there is a user
		if int(user.id) == int(employer.user.id):
			cur_user = True

	if not cur_user:
		jobs = jobs.filter(active=True)#these are the jobs you will see if you aren't the user who made the employer

	context= {
	'user':user,
	'recent_jobs':recent_jobs,
	'employer':employer,
	'employers':employers,
	'jobs':jobs,
	'cur_user':cur_user,
	}
	return render(request, 'jobboard/employer_detail.html', context)

@login_required
def add_employer(request):
	user = request.user
	employers = Employer.objects.all()
	all_jobs = Job.objects.all().order_by('-pub_date')
	recent_jobs = all_jobs.filter(active=True)[:5]
	employer = Employer(user=user)
	form = AddEmployer(request.POST or None, instance=employer)
	
	if form.is_valid():
		form.save()
		messages.success(request, 'Successfully added an employer.')
	elif form.errors:
		messages.error(request, 'Something went wrong, please try again.')
	context= {
	'user':user,
	'recent_jobs':recent_jobs,
	'employers':employers,
	'form':form,
	}
	return render(request, 'jobboard/employer_add.html', context)

@login_required
def edit_employer(request, employer_id):
	user = request.user
	employers = Employer.objects.all()
	employer = get_object_or_404(Employer, pk=employer_id)
	all_jobs = Job.objects.all().order_by('-pub_date')
	recent_jobs = all_jobs.filter(active=True)[:5]
	cur_user = False
	if(user.id > 0):#just checking to make sure there is a user
		if int(user.id) == int(employer.user.id):
			cur_user = True
		else:
			messages.error(request, 'You are unable to edit this employer as you are not the owner.')
	form = EditEmployer(request.POST or None, instance = employer)
	if cur_user:
		if form.is_valid():
			form.save()
			messages.success(request, 'Successfully edited employer.')
		elif form.errors:
			messages.error(request, 'Something went wrong, please try again.')
			#return HttpResponseRedirect(reverse('jobboard:employer_detail', args=(employer.id,)))

	context= {
	'user':user,
	'recent_jobs':recent_jobs,
	'employers':employers,
	'employer':employer,
	'form':form,
	'cur_user':cur_user,
	}
	return render(request, 'jobboard/employer_edit.html', context)

@login_required
def add_job(request):
	user = request.user
	employers = Employer.objects.all()
	all_jobs = Job.objects.all().order_by('-pub_date')
	recent_jobs = all_jobs.filter(active=True)[:5]
	form = AddJob(request.POST or None)
	form.fields['employer'].queryset = Employer.objects.all().filter(user=user)

	if form.is_valid():
		form.save()
		
		new_job=Job.objects.order_by('-pk')[0] #this seems kinda hacky
		if new_job.active:
			notify_subscribers(request, new_job.id)
			messages.success(request, 'Successfully added a job. Emails have been sent to subscribers')
		else:
			messages.success(request, 'Successfully added a job, but it is not currently active')
	elif form.errors:
		messages.error(request, 'Something went wrong, please try again.')
	context= {
	'user':user,
	'recent_jobs':recent_jobs,
	'employers':employers,
	'form':form,
	}
	return render(request, 'jobboard/job_add.html', context)

@login_required
def edit_job(request, job_id):
	user = request.user
	employers = Employer.objects.all()
	all_jobs = Job.objects.all().order_by('-pub_date')
	recent_jobs = all_jobs.filter(active=True)[:5]
	job = get_object_or_404(Job, pk=job_id)
	cur_user = False
	if int(user.id) == int(job.employer.user.id):
		cur_user = True
	form = EditJob(request.POST or None, instance=job)
	if cur_user:
		if form.is_valid():
			form.save()
			messages.success(request, 'Successfully edited job.')
		elif form.errors:
			messages.error(request, 'Something went wrong, please try again.')
			#return HttpResponseRedirect(reverse('jobboard:job_detail', args=(job.id,)))

	else:
		messages.error(request, 'You did not create this job, therefore you cannot edit it.')
	context= {
	'user':user,
	'recent_jobs':recent_jobs,
	'employers':employers,
	'job':job,
	'form':form,
	'cur_user':cur_user,
	}
	return render(request, 'jobboard/job_edit.html', context)

def add_subscriber(request):
	user = request.user
	employers = Employer.objects.all()
	all_jobs = Job.objects.all().order_by('-pub_date')
	recent_jobs = all_jobs.filter(active=True)[:5]
	form = AddSubscriber(request.POST or None)
	if form.is_valid():
		form.save()
		messages.success(request, 'You have successfully subscribed to job post emails.')
	elif form.errors:
		messages.error(request, 'Something went wrong, please try again.')
	
	context= {
	'user':user,
	'recent_jobs':recent_jobs,
	'employers':employers,
	'form':form,
	}
	return render(request, 'jobboard/subscriber_add.html', context)

def del_subscriber(request):
	user = request.user
	employers = Employer.objects.all()
	all_jobs = Job.objects.all().order_by('-pub_date')
	recent_jobs = all_jobs.filter(active=True)[:5]
	form = DelSubscriber(request.POST or None)
	if form.is_valid():
		cd = form.cleaned_data
		try:
			sub = Subscriber.objects.get(email = cd.get('email'))
			sub.delete()
			messages.success(request, 'You have been unsubscribed from emails about job listings.')
		except Subscriber.DoesNotExist:
			messages.error(request, 'The email you have entered is not currently subscribed to job listings.')
		
	elif form.errors:
		messages.error(request, 'Something went wrong, please try again.')

	context= {
	'user':user,
	'recent_jobs':recent_jobs,
	'employers':employers,
	'form':form,
	}
	return render(request, 'jobboard/subscriber_del.html', context)

def notify_subscribers(request, job_id):
	#send the mass emails
	job = Job.objects.get(pk=job_id)
	subscribers = Subscriber.objects.all()
	for subscriber in subscribers:

		template = loader.get_template('jobboard/email.txt')
		context = RequestContext(request, {'name':subscriber.name, 'title':job.title, 'employer':job.employer.name, 'job_id':job.id,})
		body = template.render(context)
		send_mail('New Job Posting: on Job Board', body, 'no-reply@jobboard.com', [subscriber.email], fail_silently=True)
	return True

def unactivate():
	today = datetime.date.today()
	jobs = Job.objects.all()
	for job in jobs:
		if job.expiry_date < today:
			job.active = False
			job.save()

