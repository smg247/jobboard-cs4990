from django import forms
from django.forms import ModelForm
from django.contrib.admin import widgets
from jobboard.models import Employer, Job, Subscriber

class AddEmployer(ModelForm):
	class Meta:
		model = Employer
		widgets = {
                        'description': forms.Textarea(attrs={'rows':3, 'cols':15}),
                }
		fields=['name','description','website','email','contact']

class AddJob(ModelForm):
	class Meta:
		model = Job
		widgets = {
                        'description': forms.Textarea(attrs={'rows':3, 'cols':15}),
                }
		exclude=('pub_date',)

class EditEmployer(ModelForm):
	class Meta:
		model = Employer
		widgets = {
                        'description': forms.Textarea(attrs={'rows':3, 'cols':15}),
                }
		fields=['name','description','website','email','contact']

class EditJob(ModelForm):
	class Meta:
		model = Job
		widgets = {
                        'description': forms.Textarea(attrs={'rows':3, 'cols':15}),
                }
		exclude=('pub_date',)

class AddSubscriber(ModelForm):
	class Meta:
		model = Subscriber
		fields=['name','email']

class DelSubscriber(forms.Form):
	email = forms.EmailField()

class Contact(forms.Form):
	name = forms.CharField(max_length=50)
	email = forms.EmailField()
	phone = forms.CharField(max_length=14)
	message = forms.CharField(widget=forms.Textarea(attrs={'rows':3, 'cols':15}))

