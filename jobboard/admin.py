from django.contrib import admin

from jobboard.models import Employer, Job, Subscriber

class EmployerAdmin(admin.ModelAdmin):
	search_fields = ['user', 'name', 'description']
	list_display = ('user', 'name', 'description',)

class JobAdmin(admin.ModelAdmin):
	search_fields = ['employer', 'title', 'description']
	list_display = ('employer', 'title', 'description',)

class SubscriberAdmin(admin.ModelAdmin):
	search_fields = ['name','email']
	list_display = ('name', 'email',)

admin.site.register(Employer, EmployerAdmin)
admin.site.register(Job, JobAdmin)
admin.site.register(Subscriber, SubscriberAdmin)
